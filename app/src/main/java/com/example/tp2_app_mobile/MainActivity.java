package com.example.tp2_app_mobile;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Parcelable;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {


    WineDbHelper wineDbHelper;
    SimpleCursorAdapter cursorAdapter;
    Cursor result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //initialise WineDbHelper
        wineDbHelper=new WineDbHelper(this);

        //
        SQLiteDatabase db = wineDbHelper.getReadableDatabase();
        wineDbHelper.winePopulate();

        //recup dans cursor tous les Vins
        result=wineDbHelper.fetchAllWines();

        cursorAdapter=new SimpleCursorAdapter(
                MainActivity.this,android.R.layout.simple_list_item_2,result,
                new String[]{WineDbHelper.COLUMN_NAME,WineDbHelper.COLUMN_LOC},new int[]{android.R.id.text1,android.R.id.text2},0);
        final ListView lv = (ListView) findViewById(R.id.listWine);
        lv.setAdapter(cursorAdapter);
        db.close();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Cursor curW =  (Cursor) parent.getItemAtPosition(position);
                Wine wine = WineDbHelper.cursorToWine(curW);
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", (Parcelable) wine);
                startActivityForResult(intent,1);

            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Wine wine = new Wine();


            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent dataIntent){
        super.onActivityResult(requestCode,resultCode,dataIntent);
        if (dataIntent != null){
            // recupération de vin
            Wine wine = dataIntent.getParcelableExtra("vin");
            // maj Bdd
            wineDbHelper.updateWine(wine);
            //maj de la liste
            cursorAdapter.changeCursor(wineDbHelper.getReadableDatabase().query(WineDbHelper.TABLE_NAME,null,null
                    , null, null, null, null));
            cursorAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
