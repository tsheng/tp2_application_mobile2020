package com.example.tp2_app_mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WineActivity extends AppCompatActivity {

    Wine vin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);


        Intent intent = getIntent();
        vin = getIntent().getParcelableExtra("wine");



            final EditText monTitre =  findViewById(R.id.title);
            monTitre.setText(vin.getTitle());

            final EditText maRegion =  findViewById(R.id.laReg);
            maRegion.setText(vin.getRegion());

            final EditText maLocalisation = findViewById(R.id.laLoc);
            maLocalisation.setText(vin.getLocalization());

            final EditText monClimat =  findViewById(R.id.leCli);
            monClimat.setText(vin.getClimate());

            final EditText maSurface =  findViewById(R.id.laSurf);
            maSurface.setText(vin.getPlantedArea());



        final Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wine newvin = new Wine();
                newvin.setClimate(monClimat.getText().toString());
                newvin.setClimate(monClimat.getText().toString());
                newvin.setPlantedArea(maSurface.getText().toString());
                newvin.setRegion(maRegion.getText().toString());
                newvin.setTitle(monTitre.getText().toString());
                newvin.setLocalization(maLocalisation.getText().toString());
                Intent intent = new Intent(WineActivity.this, MainActivity.class);
                intent.putExtra("vin",newvin);
                setResult(1,intent);
                finish();
            }
        });
    }
}
