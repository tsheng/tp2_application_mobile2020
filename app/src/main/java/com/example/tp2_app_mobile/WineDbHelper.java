package com.example.tp2_app_mobile;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();
    public static long ID = 0;
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";
    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE cellar (_id NUMERIC, name TEXT, region TEXT, localization TEXT, climate TEXT, publisher TEXT);");
        /*String CREATE_TABLES = "CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY, "
                + COLUMN_NAME  + " VARCHAR, "
                + COLUMN_WINE_REGION + " VARCHAR, "
                + COLUMN_LOC + " VARCHAR, "
                + COLUMN_CLIMATE + " VARCHAR, "
                + COLUMN_PLANTED_AREA + " VARCHAR )";
        db.execSQL(CREATE_TABLES);
        winePopulate();*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSQL = "DROP Table Livre";
        db.execSQL(strSQL);
        this.onCreate( db );
        Log.i("DATABASE ","En cours de Changement !");
    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        long rowID = 0;
        wine.setId(ID);
        ID++;
        ContentValues contentValues = new ContentValues() ;
        contentValues.put(_ID, wine.getId());
        contentValues.put(COLUMN_NAME, wine.getTitle());
        contentValues.put(COLUMN_WINE_REGION, wine.getRegion());
        contentValues.put(COLUMN_LOC, wine.getLocalization());
        contentValues.put(COLUMN_CLIMATE, wine.getClimate());
        contentValues.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        rowID = db.insert(TABLE_NAME, null, contentValues);


        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        int res =0 ;
        values.put(COLUMN_NAME,wine.getTitle());
        values.put(COLUMN_WINE_REGION,wine.getRegion());
        values.put(COLUMN_LOC,wine.getLocalization());
        values.put(COLUMN_CLIMATE,wine.getClimate());
        values.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());

        long recupId = wine.getId();
        String[] whereArgs = { wine.getTitle().toString()};
        // updating row
        // db.update(TABLE_NAME,values,WineDbHelper._ID + " == ?",whereArgs);
        db.update(TABLE_NAME,values,WineDbHelper.COLUMN_NAME+" LIKE ?",whereArgs);
        //db.update(TABLE_NAME,values,WineDbHelper._ID+" ROWID as _id ?",recupId);
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        // call db.query()
        cursor=db.query(TABLE_NAME,new String[]{_ID,COLUMN_NAME,COLUMN_WINE_REGION,COLUMN_LOC,COLUMN_CLIMATE,COLUMN_PLANTED_AREA},null,null,null,null,null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        db.close();
    }

    public void winePopulate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        //long id = Long.parseLong(cursor.getString(cursor.getColumnIndex(WineDbHelper._ID)));
        String nomVin  = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME));
        String regionVin = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_WINE_REGION ));
        String localisationVin = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_LOC));
        String climatVin = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_CLIMATE));
        String zoneVin = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_PLANTED_AREA));

        Wine wine = new Wine(nomVin, regionVin, localisationVin, climatVin, zoneVin);
        return wine;
    }
}
